package com.example.demo.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author Sanyuja Kharat
 *
 */
@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@PostMapping("/employeeinfo/saveemployee")
	public ResponseEntity<?> saveemployee(@RequestBody @Valid EmployeeDto employeeDto) throws JsonProcessingException {

		Map<String, Object> service = employeeService.saveemployeedetails(employeeDto);

		return ResponseEntity.ok(service);
	}

}
