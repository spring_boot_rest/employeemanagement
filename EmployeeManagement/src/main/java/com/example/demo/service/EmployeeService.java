package com.example.demo.service;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.utils.UtilsClass;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author Sanyuja Kharat
 *
 */
@Service
public class EmployeeService {

	@Autowired
	private UtilsClass utils;

	@Autowired
	private EmployeeRepository employeeRepository;

	public Map<String, Object> saveemployeedetails(EmployeeDto employeeDto) throws JsonProcessingException {

		Employee employee = utils.utilsmethod(employeeDto);
		employeeRepository.save(employee);

		Map<String, Object> attributes = new LinkedHashMap<>();
		attributes.put("Message", "Employee Verified successfully");
		attributes.put("EmployeetName", employee.getEmployeeName());
		attributes.put("Address", employee.getAddress());
		attributes.put("Salary", employee.getSalary());
		attributes.put("Department", employee.getDepartment());
		attributes.put("Pincode", employee.getPincode());
		attributes.put("State", employee.getState());
		attributes.put("Country", employee.getCountry());

//		attributes.entrySet().removeIf(e -> e.getValue().equals("null"));

		Set<Entry<String, Object>> entries = attributes.entrySet();

		Iterator<Entry<String, Object>> iterator = entries.iterator();

		while (iterator.hasNext()) {
			Entry<String, Object> entry = iterator.next();

			Object value = entry.getValue();

			if (Objects.isNull(value)) {
				iterator.remove();
			}

		}

		employee.setEmployeeAttributes(attributes);

		employee.serializeCustomerAttributes();

		return employee.getEmployeeAttributes();

	}

}
