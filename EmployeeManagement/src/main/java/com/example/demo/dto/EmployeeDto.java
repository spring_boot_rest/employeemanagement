package com.example.demo.dto;

import javax.validation.constraints.NotNull;

/**
 * @author Sanyuja Kharat
 *
 */
public class EmployeeDto {
	
	private int id;
	
	private String employeeName;
	private String address;
	private String salary;
	private String department;
	private String pincode;
	private String state;
	private String country;
	
	public EmployeeDto() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "EmployeeDto [id=" + id + ", employeeName=" + employeeName + ", address=" + address + ", salary="
				+ salary + ", department=" + department + ", pincode=" + pincode + ", state=" + state + ", country="
				+ country + "]";
	}

}