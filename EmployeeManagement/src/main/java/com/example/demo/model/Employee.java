package com.example.demo.model;

import java.io.IOException;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Sanyuja Kharat
 *
 */
@Entity
@Table(name = "employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "employeeName")
	private String employeeName;

	@Column(name = "address")
	private String address;

	@Column(name = "salary")
	private String salary;

	@Column(name = "department")
	private String department;

	@Column(name = "pincode")
	private String pincode;

	@Column(name = "state")
	private String state;

	@Column(name = "country")
	private String country;

	private String employeeAttributeJSON;

	@Convert(converter = HashMapConvertor.class)
	private Map<String, Object> employeeAttributes;

	public Employee() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getEmployeeAttributeJSON() {
		return employeeAttributeJSON;
	}

	public void setEmployeeAttributeJSON(String employeeAttributeJSON) {
		this.employeeAttributeJSON = employeeAttributeJSON;
	}

	public Map<String, Object> getEmployeeAttributes() {
		return employeeAttributes;
	}

	public void setEmployeeAttributes(Map<String, Object> employeeAttributes) {
		this.employeeAttributes = employeeAttributes;
	}

	public static ObjectMapper getObjectmapper() {
		return objectMapper;
	}

	private static final ObjectMapper objectMapper = new ObjectMapper();

	public void serializeCustomerAttributes() throws JsonProcessingException {
		this.employeeAttributeJSON = objectMapper.writeValueAsString(employeeAttributes);
	}

	public void deserializeCustomerAttributes() throws IOException {
		this.employeeAttributes = objectMapper.readValue(employeeAttributeJSON, Map.class);
	}

}
