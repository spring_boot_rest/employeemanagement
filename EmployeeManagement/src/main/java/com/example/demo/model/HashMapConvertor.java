package com.example.demo.model;

import java.io.IOException;
import java.util.Map;

import javax.persistence.AttributeConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HashMapConvertor implements AttributeConverter<Map<String, Object>, String>{
	
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(Map<String, Object> employeeInfo) {

        String employeeInfoJson = null;
        try {
        	
        	
        	employeeInfoJson = objectMapper.writeValueAsString(employeeInfo);
        } catch (final JsonProcessingException e) {
        }

        return employeeInfoJson;
    }

    @Override
    public Map<String, Object> convertToEntityAttribute(String employeeInfoJson) {

        Map<String, Object> employeeInfo = null;
        try {
        	employeeInfo = objectMapper.readValue(employeeInfoJson, Map.class);
        } catch (final IOException e) {
        }

        return employeeInfo;
    }

}
	


