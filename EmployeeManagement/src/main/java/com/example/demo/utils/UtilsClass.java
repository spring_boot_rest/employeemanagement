package com.example.demo.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.model.Employee;

/**
 * @author Sanyuja Kharat
 *
 */
@Component
public class UtilsClass {

	public Employee utilsmethod(EmployeeDto employeeDto) {

		Employee employee = new Employee();

//		String pattern = "^[A-Za-z\\s]*$";
//		String pattern="-/@#$%^&_+=()";
		String string = "[^a-zA-Z0-9]";

		Pattern pattern = Pattern.compile(string);
		Matcher matcher = pattern.matcher(employeeDto.getEmployeeName());
		boolean specialChar = matcher.find();

		if (specialChar) {

			employee.setEmployeeName(null);

		} else {

			employee.setEmployeeName(employeeDto.getEmployeeName());
		}

		Matcher matcher1 = pattern.matcher(employeeDto.getAddress());
		boolean specialChar1 = matcher1.find();

		if (specialChar1) {

			employee.setAddress(null);
		} else {
			employee.setAddress(employeeDto.getAddress());
		}

		Matcher matcher2 = pattern.matcher(employeeDto.getSalary());
		boolean specialchar2 = matcher2.find();

		if (specialchar2) {
			employee.setSalary(null);

		} else {
			employee.setSalary(employeeDto.getSalary());
		}

		Matcher matcher3 = pattern.matcher(employeeDto.getDepartment());
		boolean specialchar3 = matcher3.find();

		if (specialchar3) {
			employee.setDepartment(null);

		} else {

			employee.setDepartment(employeeDto.getDepartment());
		}

		Matcher matcher4 = pattern.matcher(employeeDto.getPincode());
		boolean specialchar4 = matcher4.find();

		if (specialchar4) {
			employee.setPincode(null);

		} else {

			employee.setPincode(employeeDto.getPincode());
		}

		Matcher matcher5 = pattern.matcher(employeeDto.getState());
		boolean specialchar5 = matcher5.find();

		if (specialchar5) {
			employee.setState(null);

		} else {

			employee.setState(employeeDto.getState());
		}

		Matcher matcher6 = pattern.matcher(employeeDto.getCountry());
		boolean specialchar6 = matcher6.find();

		if (specialchar6) {
			employee.setCountry(null);

		} else {

			employee.setCountry(employeeDto.getCountry());
		}

		return employee;
	}

}
